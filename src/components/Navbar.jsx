function Navbar({ headline }) {
  return (
    <nav className="navbar bg-body-tertiary">
      <div className="container-fluid">
        <a className="navbar-brand">
          <i className="bi bi-lightning-charge"></i>
          <span style={{ marginLeft: '0.5em' }}>
            Flashcards – <b>{headline}</b>
          </span>
        </a>
      </div>
    </nav>
  )
}

export default Navbar
